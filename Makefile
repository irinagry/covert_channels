all: sender receiver comp_avg

sender: sender.cpp
	g++ -pthread -o sender sender.cpp -I.

receiver: receiver.cpp
	g++ -pthread -o receiver receiver.cpp -I.

comp_avg: calc_comp_unit.cpp
	g++ -o calc_unit calc_comp_unit.cpp -I.

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ *.txt
	rm sender
	rm receiver
	rm calc_unit
	echo "" > avg_comp.h
