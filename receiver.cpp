/*
 * Challenge 1 - Irina Grigorescu SCPD
 * RECEIVER of code
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream> 
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <limits>
#include <cmath>

#include "constants.h"
#include "avg_comp.h"

//#define DEBUG

using namespace std;

int thread_on_core(int id);
void receive(int core_number, FILE *file_out);
void computational_unit();
void loop(double sec);
double get_time_sec_comp_unit();
double avg_computational_unit();
long calculate_number_of_times_in_standard_timespan(double sec);

/* 
 * Function which sets the current thread on a certain CPU core
 */
int thread_on_core(int id) {
  int no_cores = sysconf(_SC_NPROCESSORS_ONLN);
  if (id >= no_cores)
      return EINVAL;

  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(id, &cpuset);

  pthread_t current_thread = pthread_self();
  return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

/*    
 * Function which will be taken as the computational unit
 */
void computational_unit() {
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    	for (int k = 0; k < N; k++)
				i*j*k;
}
  
/* Function to create a loop of high CPU load 
 * for as many seconds as stated in the parameter
 * sec = number of seconds
 */
void loop (double sec) {
  clock_t begin = clock();

  while ((double(clock() - begin) / CLOCKS_PER_SEC) <= sec) {
    // Do something computationally intensive
    computational_unit();
  }
}

/*
 * Calculate number of times computational_unit is being calculated in 1s
 */
long calculate_number_of_times_in_standard_timespan(double sec) {
	long i = 0;

	clock_t begin = clock();
	while ((double(clock() - begin) / CLOCKS_PER_SEC) <= sec) {
		computational_unit();
		i++;
	}

	return i;
}

/*
 * Calculate average time of computational_unit to execute
 */
double avg_computational_unit() {
	int i = 0;
	double avg_comp = 0.0;

  clock_t begin = clock();
  while (i < N_AVG)
  {
    computational_unit();
    i++;
  }
  avg_comp = double(double(clock() - begin) / CLOCKS_PER_SEC) / N_AVG;

	return avg_comp;
}

double get_time_sec_comp_unit() {
	clock_t begin = clock();
	computational_unit();
	return (double(clock() - begin) / CLOCKS_PER_SEC);
}

/* 
 * Function that handles the receiving of the code
 */
void receive (int core_number, FILE *file_out) {
	//(core_number == 0) ?  thread_on_core(0) : thread_on_core(core_number - 1);
	double average_comp, exec_time;

  if (file_out == NULL) {
    cout << "error opening file" << endl;
    exit(EXIT_FAILURE);
  }

	/////////// 0
	// Initialization Process
	// Compute average
#ifndef AVG_COMP_UNIT
	average_comp = avg_computational_unit();
#else
	average_comp = AVG_COMP_UNIT;
#endif
	
	
	/////////// 1
	// Synchronization Phase
	// Commenting Synchronization Phase for now
	/*while(true) {
		clock_t start_time = clock();
		clock_t end_time;

		// Measure execution time of computational_unit
		exec_time = get_time_sec_comp_unit();
		
		// If the sync procedure begins
		if (exec_time >= (average_comp + THS)) {
			do {
				end_time = clock();
			} while(exec_time < (average_comp + THS));
			
			if (abs((double(end_time - start_time) / CLOCKS_PER_SEC) - 1.5) < EPS) {
				// found the sender's sync procedure
				break;
			}
			else
				// did not find the sender's sync procedure
				continue;
		}
	}*/
  // Sleep for 1s to distinguish between phases
	sleep(1);


	/////////// 2
	// Confirmation Phase for Receiver
	// Calculate standard_number = how often the computational_unit is being calculated in standard_timespan
	// Set standard_timespan = 1s
	int standard_time = 1;
	long standard_number = calculate_number_of_times_in_standard_timespan(standard_time);
	// If standard_time > precalculated average by a threshold, then it is not ok
	if (abs(standard_number - (1 / average_comp)) > THS_STD_NO) {
		// TODO: Make the whole process run again
#ifdef DEBUG
		cout << "Too much interference. Recommend retransmission of message" << endl;
#endif
	}
	// Sleep for 1s to distinguish between phases
	sleep(1);


	/////////// 3
	// Confirmation Phase for Sender
	// Sleep for 1s to let receiver calculate standard time for computational_unit
	// Sleep for another 1s to distinguish between phases
	sleep(2);


	/////////// 4
	// Bit Receiving Phase
	int step = 0, words = 0, parity = 0;
	char message[10];
	
	while (words < WORDS) 
	{
		while (step < (WORD + 1)) {
			// Receiving bits
			long calculated_number = calculate_number_of_times_in_standard_timespan(standard_time);
			if (calculated_number < THS_TRANS*standard_number) {
				message[step++] = '1';
				if (step != (WORD+1))
					parity++;
			}
			else
				message[step++] = '0';
		
			// Sending ACK
			if (step == (WORD + 1)) {

				// Sleep between receiving and sending ack
				sleep(1);

				if ((parity % 2) == (message[step-1] - 48)) {
					// If it is ok, parity is ok, receiver sleeps for 1s
					sleep(1);
					char c = message[step-1];
					message[step-1] = '\0';
					fputs(message, file_out);
					//fputs("\n", file_out);
#ifdef DEBUG
					cout << words << ": " << message << "|" << c << " calculated parity: " << (parity%2) << endl;
#endif
					words++;
				}
				else {
					// If it is not ok, request retransmission
					loop(1);
#ifdef DEBUG
					cout << words << ": request retransmission" << endl;
#endif
					// words is decreased, step becomes 0
					step = 0;
				}
			}

			// Sleeping between letters
			sleep(1);
		}
		// Everything ok, words increased, step becomes 0
		step = 0;
	}

}

/*
 * Main 
 */
int main (int argc, char **argv) {

	if (argc != 2) {
		cout << "Usage: " << endl << endl;
		cout << argv[0] << " <output_file_with_message>" << endl;
		return 0;
	}

	int core_number = 0;
	FILE *file_out = fopen(argv[1], "wt");

	receive(core_number, file_out);

	fclose(file_out);
	cout << "Receiver Done" << endl;

	return 0;
}
