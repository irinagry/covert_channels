// Constants for length of word, number of words
#define Q			1
#define N			7
#define WORDS		1
#define WORD		8

// Constants for sending/receiving process
#define N_AVG 100
#define THS 0.03 
#define THS_STD_NO 45000
#define EPS 0.01 
#define THS_TRANS 0.9
