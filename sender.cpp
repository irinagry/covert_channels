/*
 * Challenge 1 - Irina Grigorescu SCPD
 * SENDER of code
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream> 
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <limits>
#include <cmath>

#include "constants.h"
#include "avg_comp.h"

//#define DEBUG

using namespace std;

int thread_on_core(int id);
void send(int core_number, FILE *file_in);
void computational_unit();
void loop(double sec);
double get_time_sec_comp_unit();
double avg_computational_unit();
long calculate_number_of_times_in_standard_timespan(double sec);


/* 
 * Function which sets the current thread on a certain CPU core
 * Used for local testing (no xen)
 */
int thread_on_core(int id) {
	int no_cores = sysconf(_SC_NPROCESSORS_ONLN);
  if (id >= no_cores)
      return EINVAL;

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id, &cpuset);

	pthread_t current_thread = pthread_self();
	return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

/*
 * Function which will be taken as the computational unit
 */
void computational_unit() {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		  for (int k = 0; k < N; k++)
			  i*j*k;
}

/*
 * Calculate average time of computational_unit to execute
 */
double avg_computational_unit() {
  int i = 0;
  double avg_comp = 0.0;

  clock_t begin = clock();
  while (i < N_AVG)
  {
    computational_unit();
    i++;
  }
  avg_comp = double(double(clock() - begin) / CLOCKS_PER_SEC) / N_AVG;

  return avg_comp;
}

/*
 * Calculate number of times computational_unit is being calculated in 1s
 */
long calculate_number_of_times_in_standard_timespan(double sec) {
  long i = 0;

  clock_t begin = clock();
  while ((double(clock() - begin) / CLOCKS_PER_SEC) <= sec) {
    computational_unit();
    i++;
  }

  return i;
}

/* Function to create a loop of high CPU load 
 * for as many seconds as stated in the parameter
 * sec = number of seconds
 */
void loop (double sec) {
  clock_t begin = clock();

  while ((double(clock() - begin) / CLOCKS_PER_SEC) <= sec) {
    // Do something computationally intensive
    computational_unit();
  }

}

double get_time_sec_comp_unit() {
  clock_t begin = clock();
  computational_unit();
  return (double(clock() - begin) / CLOCKS_PER_SEC);
}

/*
 * Function that sends read bits from file
 */
void send (int core_number, FILE *file_in) {
	//(core_number == 0) ?	thread_on_core(0) : thread_on_core(core_number - 1);
	double average_comp, exec_time;

	if (file_in == NULL) {
		cout << "error opening file" << endl;
		exit(EXIT_FAILURE);
	}
	
	//clock_t begin = clock();
	//computational_unit();
	//cout << "Time: " << (double(clock() - begin) / CLOCKS_PER_SEC) << endl;


	/////////// 0
	// Initialization Process
	// Compute average (how long it takes for 1 execution of comp_unit)
#ifndef AVG_COMP_UNIT
	average_comp = avg_computational_unit();
#else
	average_comp = AVG_COMP_UNIT;
#endif

	/////////// 1
	// Synchronization Phase
	// Execute computational_unit for 1.5s as a sync phase procedure
	// Will comment 
	//loop(1.5);
	// Sleep for 1s to distinguish between phases
	sleep(1);


	/////////// 2
	// Confirmation Phase for Receiver
	// Sleep for 1s to let receiver calculate standard time for computational_unit
	// Sleep for another 1s to distinguish between phases
	sleep(2);


	/////////// 3
	// Confirmation Phase for Sender
	int standard_time = 1;
	long standard_number = calculate_number_of_times_in_standard_timespan(standard_time);
  // If standard_time > precalculated average by a threshold, then it is not ok
  if (abs(standard_number - (1 / average_comp)) > THS_STD_NO) {
    // TODO: Make the whole process run again
#ifdef DEBUG
	cout << "Too much interference. Recommend retransmission of message" << endl;
#endif
  }
  // Sleep for 1s to distinguish between phases
  sleep(1);


	/////////// 4 
	// Bit Transmission Phase
	int c, message_length = 0, i = 0, parity = 0; char message[100];
	// Read file first
	while ((c = fgetc(file_in)) != EOF) {
		if (c == '1')
			message[message_length] = '1';
		else if (c == '0')
			message[message_length] = '0';
		message_length++;
	}
	message_length--;
	
	// Send bits
  while (i < message_length) {
		if (message[i] == '1') {
#ifdef DEBUG
			cout << message[i];
#endif
			parity++; i++;
			// Loop for 1 sec
			loop(1);
		}
		else if (message[i] == '0') {
#ifdef DEBUG
			cout << message[i];
#endif
			i++;
			// Sleep for 1 sec
			sleep(1);
		}
		
		// Verify if a block of 8-bits has been read (FSC)
		if ((i % WORD) == 0) {
			// Sending Parity (FSC)
			if ((parity % 2) == 0) {
#ifdef DEBUG
				cout << (parity%2);
#endif
				sleep(1);
			}
			else {
#ifdef DEBUG
				cout << (parity%2);
#endif
				loop(1);
			}
			parity = 0;
#ifdef DEBUG
			cout << endl;
#endif

			// Sleep between sending and receving ack
			sleep(1);


			// Wait for confirmation
			long calculated_number = calculate_number_of_times_in_standard_timespan(standard_time);
			// If it is not ok, receiver computed for 1s
			if (calculated_number < THS_TRANS*standard_number) {
				i -= WORD;
			} 
			// If it is ok, receiver slept for 1s
			else {
			}
		}

		// Sleeping between letters
		sleep(1);
	} 	

}

/*
 * Main 
 */
int main (int argc, char **argv) {

	if (argc != 2) {
		cout << "Usage: " << endl << endl;
		cout << argv[0] << " <input_file_with_message>" << endl;
		return 0;
	}

	int core_number = 0;
	FILE *file_in = fopen(argv[1], "rt");

	send(core_number, file_in);

	fclose(file_in);
	cout << "Sender Done" << endl;	

	return 0;
}
