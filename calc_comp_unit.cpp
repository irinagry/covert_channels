/*
 * Challenge 1 - Irina Grigorescu SCPD
 * COMPUTE Average
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream> 
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <limits>
#include <cmath>

#include "constants.h"

using namespace std;

/*
 * Function which will be taken as the computational unit
 */
void computational_unit() {
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      for (int k = 0; k < N; k++)
        i*j*k;
}

/*  
 * Calculate average time of computational_unit to execute
 * and store it to file
 */     
double avg_computational_unit() {
  int i = 0;
  double avg_comp = 0.0;

  clock_t begin = clock();
  while (i < N_AVG)
  {
    computational_unit();
    i++;
  }
  avg_comp = double(double(clock() - begin) / CLOCKS_PER_SEC) / N_AVG;
  
  return avg_comp;
} 

int main (int argc, char **argv) {

	double average_comp = avg_computational_unit();
	
	char command[100];
	sprintf(command, "/bin/echo \"#define AVG_COMP_UNIT %.10lf \" > avg_comp.h", (double)average_comp);

	system(command);

	return 0;
}
